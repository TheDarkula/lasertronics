use crate::libretro::enums::CurrentGame;

use std::fs::File;
use std::io::{BufReader, BufRead};

use ::log::{debug, error};

pub trait ConfigParsing {
    fn config_to_current_game(&self) -> CurrentGame;
}

impl ConfigParsing for String {
    fn config_to_current_game(&self) -> CurrentGame {
        debug!("In config_to_current_game()");

        const GAME_NAME_CONFIG_FILE: &'static str = "game_name";

        let mut game_path_clone = self.clone();
        game_path_clone.push_str(GAME_NAME_CONFIG_FILE);

        let input_file: File = match File::open(game_path_clone) {
            Ok(input) => input,
            Err(error) => {
                error!("Could not open the file 'game_name' in the given path. Please ensure this file exists: {}", error);
                std::process::exit(1);
            },
        };

        let buffered: BufReader<File> = BufReader::new(input_file);

        let single_line_game_name: String = match buffered.lines().take(1).next() {
            Some(line) => match line {
                Ok(game_string) => game_string,
                Err(error) => {
                    error!("Could not read line: {}", error);
                    std::process::exit(1);
                },
            },
            None => {
                error!("Could not read config file.");
                std::process::exit(1);
            },
        };

        match single_line_game_name.as_str() {
            "Dragon's Lair" => CurrentGame::DragonsLair,
            "Dragon's Lair II" => CurrentGame::DragonsLairII,
            "Space Ace" => CurrentGame::SpaceAce,
            _ => {
                const SPECIFY_GAME_ERROR: &'static str = "Game not specified in the game_name config file.
                Please have only one line with one of the following:
                Dragon's Lair
                Dragon's Lair II
                Space Ace";

                error!("{}", SPECIFY_GAME_ERROR);
                std::process::exit(1);
            },
        }
    }
}