use crate::libretro::enums::{CurrentGame, Debugging, RetroHwContextType};
use crate::libretro::types::{AudioSampleFunctionPointer, AudioSampleBatchFunctionPointer,
                             EnvironmentFunctionPointer, GetCurrentFramebufferFunctionPointer,
                             GetProcAddressFunctionPointer, InputPollFunctionPointer,
                             InputStateFunctionPointer, ResetFunctionPointer,
                             VideoRefreshFunctionPointer};


use std::ffi::CString;


#[derive(Clone, Debug)]
#[repr(C)]
pub struct RetroFramebuffer {
    // The framebuffer which the core can render into. Set by frontend in
    // GET_CURRENT_SOFTWARE_FRAMEBUFFER. The initial contents of data are unspecified.
    pub data: *mut libc::c_void,

    // The framebuffer width used by the core. Set by core.
    pub width: libc::c_uint,

    // The framebuffer height used by the core. Set by core.
    pub height: libc::c_uint,

    // The number of bytes between the beginning of a scanline, and beginning of the
    // next scanline. Set by frontend in GET_CURRENT_SOFTWARE_FRAMEBUFFER.
    pub pitch: libc::size_t,

    // The pixel format the core must use to render into data. This format could
    // differ from the format used in SET_PIXEL_FORMAT. Set by frontend in
    // GET_CURRENT_SOFTWARE_FRAMEBUFFER. A value from enum PixelFormat.
    pub format: libc::c_uint,

    // How the core will access the memory in the framebuffer. MEMORY_ACCESS_*
    // flags. Set by core.
    pub access_flags: libc::c_uint,

    // Flags telling core how the memory has been mapped. MEMORY_TYPE_* flags.
    // Set by frontend in GET_CURRENT_SOFTWARE_FRAMEBUFFER.
    pub memory_flags: libc::c_uint,
}

#[derive(Clone, Debug)]
pub struct Callbacks {
    pub video_refresh: Option<VideoRefreshFunctionPointer>,
    pub audio_sample: Option<AudioSampleFunctionPointer>,
    pub audio_sample_batch: Option<AudioSampleBatchFunctionPointer>,
    pub input_poll: Option<InputPollFunctionPointer>,
    pub input_state: Option<InputStateFunctionPointer>,
    pub environment: Option<EnvironmentFunctionPointer>,
}

impl Callbacks {
    fn new() -> Self {
        Callbacks {
            video_refresh: None,
            audio_sample: None,
            audio_sample_batch: None,
            input_poll: None,
            input_state: None,
            environment: None,
        }
    }
}

impl Default for Callbacks {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Clone, Debug)]
#[repr(C)]
pub struct RetroSystemInfo {
    pub library_name: *const libc::c_char,
    pub library_version: *const libc::c_char,
    pub valid_extensions: *const libc::c_char,
    pub need_fullpath: bool,
    pub block_extract: bool,
}

#[derive(Clone, Debug)]
#[repr(C)]
pub struct RetroGameGeometry {
    // Nominal video width of game.
    pub base_width: libc::c_uint,

    // Nominal video height of game.
    pub base_height: libc::c_uint,

    // Maximum possible width of game.
    pub max_width: libc::c_uint,

    // Maximum possible height of game.
    pub max_height: libc::c_uint,

    // Nominal aspect ratio of game. If aspect_ratio is <= 0.0, an aspect ratio of
    // base_width / base_height is assumed. A frontend could override this setting, if
    // desired.
    pub aspect_ratio: f32,
}

impl RetroGameGeometry {
    fn new() -> Self {
        RetroGameGeometry {
            base_width: 0,
            base_height: 0,
            max_width: 0,
            max_height: 0,
            aspect_ratio: 0.0,
        }
    }
}

impl Default for RetroGameGeometry {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Clone, Debug)]
#[repr(C)]
pub struct RetroSystemTiming {
    // FPS of video content.
    pub fps: f64,

    // Sampling rate of audio.
    pub sample_rate: f64,
}

impl RetroSystemTiming {
    fn new() -> Self {
        RetroSystemTiming {
            fps: 0.0,
            sample_rate: 23000.0,
        }
    }
}

impl Default for RetroSystemTiming {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Clone, Debug)]
#[repr(C)]
pub struct RetroSystemAvInfo {
    pub geometry: RetroGameGeometry,
    pub timing: RetroSystemTiming,
}

impl RetroSystemAvInfo {
    fn new() -> Self {
        RetroSystemAvInfo {
            geometry: RetroGameGeometry::default(),
            timing: RetroSystemTiming::default(),
        }
    }
}

impl Default for RetroSystemAvInfo {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Clone, Debug)]
#[repr(C)]
pub struct RetroGameInfo {
    // Path to game, UTF-8 encoded. Usually used as a reference. May be NULL if rom
    // was loaded from stdin or similar. retro_system_info::need_fullpath guaranteed
    // that this path is valid.
    pub path: *const libc::c_char,

    // Memory buffer of loaded game. Will be NULL if need_fullpath was set.
    pub data: *const libc::c_void,

    // Size of memory buffer.
    pub size: libc::size_t,

    // String of implementation specific meta-data.
    pub meta: *const libc::c_char,
}

unsafe impl Send for RetroGameInfo {}

impl RetroGameInfo {
    fn new() -> Self {
        RetroGameInfo {
            path: CString::new("").expect("Could not create default path for RetroGameInfo").into_raw(),
            data: 0 as *const libc::c_void,
            size: 0,
            meta: CString::new("").expect("Could not create default meta for RetroGameInfo").into_raw(),
        }
    }
}

impl Default for RetroGameInfo {
    fn default() -> Self {
        Self::new()
    }
}

pub struct GameState {
    pub game_path: Option<String>,
    pub current_game: Option<CurrentGame>,
    // Fields that need to be implemented:
    // number of lives
    // items obtained (Option<CurrentItems>, because some games do not have items)
    // current room
}

impl GameState {
    pub fn new() -> Self {
        GameState {
            game_path: Some(String::from("")),
            current_game: None,
        }
    }

    pub fn on_get_memory_data(&mut self, id: libc::c_uint) -> *mut libc::c_void {
        self.memory_data( id )
            .map( |d| d as *mut _ as *mut libc::c_void )
            .unwrap_or( std::ptr::null_mut() )
    }

    pub fn memory_data(&mut self, id: libc::c_uint) -> Option< &mut [u8] > {
        // match id {
        //     crate::RETRO_MEMORY_SAVE_RAM => self.core.save_memory(),
        //     crate::RETRO_MEMORY_RTC => self.core.rtc_memory(),
        //     crate::CRATE_MEMORY_SYSTEM_RAM => self.core.system_memory(),
        //     crate::RETRO_MEMORY_VIDEO_RAM => self.core.video_memory(),
        //     _ => unreachable!(),
        // }
        panic!("In memory_data()");
    }

    pub fn on_get_memory_size(&mut self, id: libc::c_uint ) -> libc::size_t {
        self.memory_data( id )
            .map( |d| d.len() as libc::size_t )
            .unwrap_or( 0 )
    }
}

impl Default for GameState {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Clone, Debug)]
#[repr(C)]
pub struct RetroHwRenderCallback {
    pub context_type: RetroHwContextType,
    pub context_reset: ResetFunctionPointer,
    pub get_current_framebuffer: GetCurrentFramebufferFunctionPointer,
    pub get_proc_address: GetProcAddressFunctionPointer,
    pub depth: bool,
    pub stencil: bool,
    pub bottom_left_origin: bool,
    pub version_major: libc::c_uint,
    pub version_minor: libc::c_uint,
    pub cache_context: bool,
    pub context_destroy: ResetFunctionPointer,
    pub debug_context: bool,
}