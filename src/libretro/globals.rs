use crate::libretro::functions::{context_destroy, dummy_get_current_framebuffer, dummy_get_proc_address, reset};
use crate::libretro::structs::{Callbacks, GameState, RetroGameInfo, RetroHwRenderCallback, RetroSystemAvInfo};
use crate::libretro::enums::{Debugging, RetroHwContextType};

use std::sync::{Arc, Mutex, RwLock};

use lazy_static::lazy_static;


lazy_static! {
    pub static ref GAME_STATE: Arc<RwLock<GameState>> = Arc::new(RwLock::new(GameState::default()));
    pub static ref CALLBACKS: Arc<RwLock<Callbacks>> = Arc::new(RwLock::new(Callbacks::default()));
    pub static ref RETRO_SYSTEM_AV_INFO: Arc<RwLock<RetroSystemAvInfo>> = Arc::new(RwLock::new(RetroSystemAvInfo::default()));
    pub static ref RETRO_GAME_INFO: Arc<Mutex<RetroGameInfo>> = Arc::new(Mutex::new(RetroGameInfo::default()));
}

pub static mut RETRO_HW_RENDER_CALLBACK_INSTANCE: RetroHwRenderCallback = RetroHwRenderCallback {
    context_type: RetroHwContextType::OpenGlCore,
    context_reset: reset,
    get_current_framebuffer: dummy_get_current_framebuffer,
    get_proc_address: dummy_get_proc_address,
    depth: false,
    stencil: false,
    bottom_left_origin: true,
    version_major: 3,
    version_minor: 3,
    cache_context: false,
    context_destroy: context_destroy,
    debug_context: false,
};