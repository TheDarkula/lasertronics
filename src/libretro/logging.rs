use crate::libretro::enums::Debugging;
use ::log::SetLoggerError;
use simplelog::{CombinedLogger, Config, LevelFilter, SharedLogger, TerminalMode, TermLogger};

pub fn initialise_logging(log_level: Debugging) {
    let log_level: LevelFilter = match log_level {
        Debugging::On => LevelFilter::Debug,
        Debugging::Off => LevelFilter::Info,
    };
    
    let terminal_logger: Box<TermLogger> = TermLogger::new(log_level, Config::default(), TerminalMode::Mixed);
    
    let logging_configuration: Vec<Box<dyn SharedLogger>> = vec![terminal_logger];

    let logger: Result<(), SetLoggerError> = CombinedLogger::init(logging_configuration);

    match logger {
        Ok(_) => {},
        Err(error) => {
            eprintln!("Initialising logging failed: {}", error);
        },
    }
}