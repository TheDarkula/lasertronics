use crate::libretro::enums::Debugging;
use crate::libretro::constants::{RETRO_API_VERSION, RETRO_ENVIRONMENT_SET_HW_RENDER, RETRO_REGION_NTSC};
use crate::libretro::globals::{CALLBACKS, GAME_STATE, RETRO_HW_RENDER_CALLBACK_INSTANCE, RETRO_SYSTEM_AV_INFO};
use crate::libretro::logging;
use crate::libretro::structs::{RetroGameInfo, RetroSystemAvInfo, RetroSystemInfo};
use crate::libretro::types::{AudioSampleFunctionPointer, AudioSampleBatchFunctionPointer,
                             EnvironmentFunctionPointer, InputPollFunctionPointer,
                             InputStateFunctionPointer, VideoRefreshFunctionPointer};
use crate::libretro::traits::ConfigParsing;

use std::ffi::{CStr, CString};

use ::log::{debug, error};

// The order of function calls retroarch will make are:
// 1) retro_get_system_info()
// 2) retro_set_environment()
// 3) retro_set_*()
// 4) retro_init()
// 5) retro_run()
// ?) retro_deinit()


#[no_mangle]
pub extern "C" fn retro_get_system_info(info: *mut RetroSystemInfo) {
    println!("In retro_get_system_info()");

    let info: &mut RetroSystemInfo = unsafe {&mut *info };

    let name: CString = CString::new("LaserTronics").expect("Could not create name CString");
    info.library_name = name.as_ptr();

    let version: CString = CString::new(env!("CARGO_PKG_VERSION")).expect("Could not create version CString");
    info.library_version = version.as_ptr();

    let extensions: CString = CString::new("").expect("Could not create extensions CString");
    info.valid_extensions = extensions.as_ptr();

    info.need_fullpath = true;

    info.block_extract = true;
}

#[no_mangle]
pub unsafe extern "C" fn retro_set_environment(callback: EnvironmentFunctionPointer) {
    println!("In retro_set_environment()");

    let callbacks_clone = CALLBACKS.clone();
    let mut callbacks_write = callbacks_clone.write().expect("Could not get a write lock on CALLBACKS in retro_set_environment()");

    callbacks_write.environment = Some(callback);

    let environment: EnvironmentFunctionPointer = callbacks_write.environment.expect("Environment callback was not set.");

    // let pixel_format: *mut libc::c_void = &mut crate::libretro::enums::RetroPixelFormat::RGB565 as *mut _ as *mut libc::c_void;
    let pixel_format: *mut libc::c_void = &mut crate::libretro::enums::RetroPixelFormat::ARGB8888 as *mut _ as *mut libc::c_void;
    environment(crate::libretro::constants::RETRO_ENVIRONMENT_SET_PIXEL_FORMAT, pixel_format);

    let instance_pointer: *mut libc::c_void = &mut RETRO_HW_RENDER_CALLBACK_INSTANCE as *mut _ as *mut libc::c_void;
    environment(RETRO_ENVIRONMENT_SET_HW_RENDER, instance_pointer);


    let debug_flag: &'static str = "LASERTRONICS_DEBUG";
    match std::env::var(debug_flag) {
        Ok(_) => {
            logging::initialise_logging(Debugging::On);
            debug!("The debugging flag is set.");
            debug!("Verbose logging will be printed.");
        },
        Err(_) => logging::initialise_logging(Debugging::Off),
    }
}

pub extern "C" fn dummy_get_current_framebuffer() -> libc::uintptr_t {
    panic!("Called missing get_current_framebuffer callback");
}

pub extern "C" fn dummy_get_proc_address(_: *const libc::c_char) -> *const libc::c_void {
    panic!("Called missing get_proc_address callback");
}

pub extern "C" fn context_destroy() {
    panic!("Called missing get_proc_address callback");
}

pub extern "C" fn reset() {
    // panic!("Called missing reset callback");
}

#[no_mangle]
pub extern "C" fn retro_init() {
    debug!("In retro_init()");
}

#[no_mangle]
pub extern "C" fn retro_deinit() {
    debug!("In retro_deinit().");
}

#[no_mangle]
pub extern "C" fn retro_api_version() -> libc::c_uint {
    debug!("In retro_api_version");

    RETRO_API_VERSION
}

#[no_mangle]
pub extern "C" fn retro_get_system_av_info(info: *mut RetroSystemAvInfo) {
    debug!("In retro_get_system_av_info()");

    let info: &mut RetroSystemAvInfo = unsafe {&mut *info };

    let system_av_info_clone = RETRO_SYSTEM_AV_INFO.clone();
    let system_av_info = system_av_info_clone.read().expect("Could not get a read lock on RETRO_SYSTEM_AV_INFO in retro_get_system_av_info()");


    info.geometry.base_width = system_av_info.geometry.base_width as libc::c_uint;
    info.geometry.base_height = system_av_info.geometry.base_height as libc::c_uint;
    info.geometry.max_width = system_av_info.geometry.max_width as libc::c_uint;
    info.geometry.max_height = system_av_info.geometry.max_height as libc::c_uint;
    info.geometry.aspect_ratio = system_av_info.geometry.aspect_ratio;
    info.timing.fps = system_av_info.timing.fps;
    info.timing.sample_rate = system_av_info.timing.sample_rate;
}

#[no_mangle]
pub extern "C" fn retro_set_video_refresh(callback: VideoRefreshFunctionPointer) {
    debug!("In retro_set_video_refresh()");

    let callbacks_clone = CALLBACKS.clone();
    let mut callbacks_write = callbacks_clone.write().expect("Could not get a write lock on CALLBACKS in retro_set_video_refresh()");

    callbacks_write.video_refresh = Some(callback);
}

#[no_mangle]
pub extern "C" fn retro_set_audio_sample(callback: AudioSampleFunctionPointer) {
    debug!("In retro_set_audio_sample()");

    let callbacks_clone = CALLBACKS.clone();
    let mut callbacks_write = callbacks_clone.write().expect("Could not get a write lock on CALLBACKS in retro_set_audio_sample()");

    callbacks_write.audio_sample = Some(callback);
}

#[no_mangle]
pub extern "C" fn retro_set_audio_sample_batch(callback: AudioSampleBatchFunctionPointer) {
    debug!("In retro_set_audio_sample_batch()");

    let callbacks_clone = CALLBACKS.clone();
    let mut callbacks_write = callbacks_clone.write().expect("Could not get a write lock on CALLBACKS in retro_set_audio_sample_batch()");

    callbacks_write.audio_sample_batch = Some(callback);
}

#[no_mangle]
pub extern "C" fn retro_set_input_poll(callback: InputPollFunctionPointer) {
    debug!("In retro_set_input_poll()");

    let callbacks_clone = CALLBACKS.clone();
    let mut callbacks_write = callbacks_clone.write().expect("Could not get a write lock on CALLBACKS in retro_set_input_poll()");

    callbacks_write.input_poll = Some(callback);
}

#[no_mangle]
pub extern "C" fn retro_set_input_state(callback: InputStateFunctionPointer) {
    debug!("In retro_set_input_state()");

    let callbacks_clone = CALLBACKS.clone();
    let mut callbacks_write = callbacks_clone.write().expect("Could not get a write lock on CALLBACKS in retro_set_input_state()");

    callbacks_write.input_state = Some(callback);
}

#[no_mangle]
pub extern "C" fn retro_set_controller_port_device(port: libc::c_uint, device: libc::c_uint) {
    debug!("In retro_set_controller_port_device()");
}

#[no_mangle]
pub extern "C" fn retro_reset() {
    debug!("In retro_reset()");
}

#[no_mangle]
pub unsafe extern "C" fn retro_run() {
    debug!("In retro_run()");

    let callbacks_clone = CALLBACKS.clone();
    let callbacks_read = callbacks_clone.read().expect("Could not get a read lock on CALLBACKS in retro_run()");
    let input_poll: InputPollFunctionPointer = callbacks_read.input_poll.expect("Input poll callback was not set.");
    input_poll();

    let video_path_variable: &'static str = "VIDEO_PATH";
    let video_path: String = match std::env::var(video_path_variable) {
        Ok(path) => path,
        Err(error) => {
            error!("Please set VIDEO_PATH to the path to an mkv file: {}", error);
            std::process::exit(1);
        },
    };

    let audio_sample_batch: AudioSampleBatchFunctionPointer = callbacks_read.audio_sample_batch.expect("Audio sample batch callback was not set.");

    let video_refresh: VideoRefreshFunctionPointer = callbacks_read.video_refresh.expect("Video refresh callback was not set.");

    crate::gstreamer::output_audio_and_video_streams(&video_path, audio_sample_batch, video_refresh);
}

#[no_mangle]
pub extern "C" fn retro_serialize_size() -> libc::size_t {
    debug!("In retro_serialize_size()");

    0
}

#[no_mangle]
pub extern "C" fn retro_serialize(data: *mut libc::c_void, size: libc::size_t) -> bool {
    debug!("In retro_serialize()");

    false
}

#[no_mangle]
pub extern "C" fn retro_unserialize(data: *const libc::c_void, size: libc::size_t) -> bool {
    debug!("In retro_unserialize()");

    false
}

#[no_mangle]
pub extern "C" fn retro_cheat_reset() {
    debug!("In retro_cheat_reset");
}

#[no_mangle]
pub extern "C" fn retro_cheat_set(index: libc::c_uint, is_enabled: bool, code: *const libc::c_char) {
    debug!("In retro_cheat_set()");
}

#[no_mangle]
pub unsafe extern "C" fn retro_load_game(game: *const RetroGameInfo) -> bool {
    debug!("In retro_load_game()");

    let game: Option<&RetroGameInfo> = match game.is_null() {
        true => None,
        false => Some(&*game),
    };

    match game {
        Some(retro_game_info) => {
            match retro_game_info.path.is_null() {
                true => {
                    error!("No game path specified. Please input a path to game files.");
                    error!("This should not happen, because the need_fullpath field is set to true.");
                    error!("This is likely a retroarch bug.");
                    unreachable!();
                },
                false => {
                    let game_state_clone = GAME_STATE.clone();
                    let mut game_state = game_state_clone.write().expect("Could not get a write lock on GAME_STATE in retro_load_game()");

                    let game_path_cstr: &CStr = CStr::from_ptr(retro_game_info.path);
                    let game_path: String = game_path_cstr.to_str().expect("Could not extract game_path in retro_load_game()").trim().to_string();

                    game_state.current_game = Some(game_path.config_to_current_game());

                    game_state.game_path = Some(game_path);

                    true
                },
            }
        },
        None => {
            error!("RetroGameInfo could not be loaded. This should not happen.");
            error!("This is likely a retroarch bug.");
            unreachable!();
        },
    }
}

#[no_mangle]
pub extern "C" fn retro_load_game_special(game_type: libc::c_uint, info: *const RetroGameInfo, num_info: libc::size_t) -> bool {
    debug!("retro_load_game_special() is unimplemented.");

    false
}

#[no_mangle]
pub unsafe extern "C" fn retro_unload_game() {
    debug!("In retro_unload_game()");
}

#[no_mangle]
pub extern "C" fn retro_get_region() -> libc::c_uint {
    debug!("In retro_get_region()");

    RETRO_REGION_NTSC
}

#[no_mangle]
pub extern "C" fn retro_get_memory_data(id: libc::c_uint) -> *mut libc::c_void {
    debug!("In retro_get_memory_data()");

    // let game_state_clone = GAME_STATE.clone();
    // let mut game_state_write = game_state_clone.write().unwrap();
    //
    // game_state_write.on_get_memory_data(id)

    // I could just return std::ptr::null_mut() here.

    std::ptr::null_mut()
}

#[no_mangle]
pub unsafe extern "C" fn retro_get_memory_size(id: libc::c_uint) -> libc::size_t {
    debug!("In retro_get_memory_size()");

    // let game_state_clone = GAME_STATE.clone();
    // let mut game_state_write = game_state_clone.write().unwrap();
    //
    // game_state_write.on_get_memory_size(id)

    // I could just return "0" here.
    0
}