#[derive(Debug)]
pub enum CurrentGame {
    DragonsLair,
    DragonsLairII,
    SpaceAce,
}

#[derive(Debug)]
pub enum Debugging {
    On,
    Off,
}

#[derive(Clone, Debug)]
#[repr(C)]
pub enum RetroHwContextType {
    None = 0,
    OpenGl = 1,
    OpenGlEs2 = 2,
    OpenGlCore = 3,
    OpenGlEs3 = 4,
    OpenGlEsVersion = 5,
    Vulkan = 6,
}

#[derive(Clone, Debug)]
#[repr(C)]
pub enum RetroPixelFormat {
    // ARGB1555, native endian.
    // Alpha bit has to be set to 0.
    // This pixel format is default for compatibility concerns only.
    // If a 15/16-bit pixel format is desired, consider using RGB565.
    ARGB1555 = 0,

    // ARGB8888, native endian.
    // Alpha bits are ignored.
    ARGB8888 = 1,

    // RGB565, native endian.
    // This pixel format is the recommended format to use if a 15/16-bit
    // format is desired as it is the pixel format that is typically
    // available on a wide range of low-power devices.
    //
    // It is also natively supported in APIs like OpenGL ES.
    RGB565 = 2,
}