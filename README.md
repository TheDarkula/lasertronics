# LaserTronics

### Running
In the specified video path, you will need one text file called `game_name` with one of the following as its contents:
```
Dragon's Lair
Dragon's Lair II
Space Ace
```

An example file can be created with the following command:
```
echo "Dragon's Lair" > game_name
```

### Developing
Run:
```
cargo build
```

Then:
```
retroarch -f -L ./target/debug/liblasertronics.so /path/to/video/files
```


### Testing

```
env VIDEO_PATH=/path/to/test/mkv/file retroarch -f -L ./target/debug/liblasertronics.so /path/to/video/files
```

### Debugging
Run with:
`LASERTRONICS_DEBUG=1`
```
env LASERTRONICS_DEBUG=1 retroarch -f -L ./target/debug/liblasertronics.so /path/to/video/files
```